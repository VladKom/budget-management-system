export default {
  install(Vue, options) {
    Vue.prototype.$message = function(html) {
      window.M.toast({html, classes: 'rounded'})
      console.log(options);
      
    }

    Vue.prototype.$error = function(html) {
      window.M.toast({html: `[Error]: ${html}`,  classes: 'rounded'})
    }
  }
}
