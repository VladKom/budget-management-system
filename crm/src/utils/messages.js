export default {
  'logout': 'You are logged out',
  'login': 'Log in to get started',
  'auth/user-not-found': 'User with such email does not exist',
  'auth/wrong-password': 'Wrong password',
  'auth/email-already-in-use': 'Email is already in use'
}
